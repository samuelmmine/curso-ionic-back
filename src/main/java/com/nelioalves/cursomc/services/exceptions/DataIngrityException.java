package com.nelioalves.cursomc.services.exceptions;

public class DataIngrityException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	
	public DataIngrityException(String msg) {
		super(msg);
	}
	
	public DataIngrityException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
